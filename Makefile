all:

install:
	install -Dm755 extra/arch-chroot $(DESTDIR)/usr/bin/arch-chroot
	install -Dm755 extra/genfstab $(DESTDIR)/usr/bin/genfstab
	install -Dm755 arch-installer.sh $(DESTDIR)/usr/bin/arch-anywhere
	install -Dm644 etc/arch-anywhere.conf $(DESTDIR)/etc/arch-anywhere.conf
	install -Dm644 etc/arch-anywhere.service $(DESTDIR)/lib/systemd/system/arch-anywhere.service
	mkdir -p $(DESTDIR)/usr/share/arch-anywhere
	cp -r hooks $(DESTDIR)/usr/share/arch-anywhere
	cp -r lang $(DESTDIR)/usr/share/arch-anywhere
	mkdir -p $(DESTDIR)/usr/lib/arch-anywhere
	cp lib/*.sh $(DESTDIR)/usr/lib/arch-anywhere

uninstall:
	rm -f $(DESTDIR)/usr/bin/arch-chroot
	rm -f $(DESTDIR)/usr/bin/genfstab
	rm -f $(DESTDIR)/usr/bin/arch-anywhere
	rm -rf $(DESTDIR)/usr/share/arch-anywhere
	rm -rf $(DESTDIR)/usr/lib/arch-anywhere

