#!/bin/bash
###############################################################
### Arch Linux Anywhere Install Script
### configure_connection.sh
###
### Copyright (C) 2017 Dylan Schacht
###
### By: Dylan Schacht (deadhead)
### Email: deadhead3492@gmail.com
### Webpage: http://arch-anywhere.org
###
### Any questions, comments, or bug reports may be sent to above
### email address. Enjoy, and keep on using Arch.
###
### License: GPL v2.0
###############################################################

check_connection() {
	op_title="$welcome_op_msg"
	if ! (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$intro_msg" 10 60) then
		reset ; exit
	fi

    if [ "$config_network" == "true" ]; then
        if (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$wifi_msg0" 10 60) then
            #TODO: Add network connection checking
            ceni
            echo "$(date -u "+%F %H:%M") : Configure Connection" >> "$log"
        fi
    else
        dialog --ok-button "$ok" --msgbox "\n$connect_err0" 10 60
        echo "$(date -u "+%F %H:%M") : Skip to connect to network." >> "$log"
        setterm -background black -store ; reset ; echo -e "$connect_err1" | sed 's/\\Z1//;s/\\Zn//'
    fi

}
