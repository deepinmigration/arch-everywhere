#!/bin/bash
###############################################################
###
### Copyright (C) 2017 Dylan Schacht
###
### By: Dylan Schacht (deadhead)
### Email: deadhead3492@gmail.com
### Webpage: http://arch-anywhere.org
###
### Any questions, comments, or bug reports may be sent to above
### email address. Enjoy, and keep on using Arch.
###
### License: GPL v2.0
###############################################################
install_base() {

	op_title="$install_op_msg"
	until "$INSTALLED"
	  do
		if (dialog --yes-button "$install" --no-button "$cancel" --yesno "\n$install_var" "$height" 65); then
			echo "$(date -u "+%F %H:%M") : Begin base install" >> "$log"
			SQUASHFS="/lib/live/mount/medium/live/filesystem.squashfs"
			while [ ! -f "${SQUASHFS}" ];do
				dialog --ok-button "$ok" --msgbox "\n${SQUASHFS} not exits" 10 60
			done
			rm -f /tmp/unsquashfs.progress
    
			(deepin-installer-unsquashfs --dest "$ARCH" --progress /tmp/unsquashfs.progress "${SQUASHFS}"; echo "$?" > /tmp/ex_status) &>> "$log" &
            (while test "$int" != "100"
            do
                sleep 0.5
                int=`tail -1 /tmp/unsquashfs.progress 2>/dev/null || echo 0`
                echo $int
                sleep 1
            done) | dialog --gauge "\nUnsquashfsing the filesystem to target" 10 79 0
			genfstab -U -p "$ARCH" >> "$ARCH"/etc/fstab
    
			if [ $(</tmp/ex_status) -eq "0" ]; then
				INSTALLED=true
				echo "$(date -u "+%F %H:%M") : Install Complete" >> "$log"
			else
				dialog --ok-button "$ok" --msgbox "\n$failed_msg" 10 60
				echo "$(date -u "+%F %H:%M") : Install failed: please report to developer" >> "$log"
				reset ; tail "$log" ; exit 1
			fi
    
			case "$bootloader" in
				grub) grub_config ;;
				syslinux) syslinux_config ;;
				systemd-boot) systemd_config ;;
				efistub) efistub_config ;;
			esac
    
			echo "$(date -u "+%F %H:%M") : Configured bootloader: $bootloader" >> "$log"
		else
			if (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$exit_msg" 10 60) then
				main_menu
			fi
		fi
	done

}
