#!/bin/bash
###############################################################
### Arch Linux Anywhere Install Script
### configure_base.sh
###
### Copyright (C) 2017 Dylan Schacht
###
### By: Dylan Schacht (deadhead)
### Email: deadhead3492@gmail.com
### Webpage: http://arch-anywhere.org
###
### Any questions, comments, or bug reports may be sent to above
### email address. Enjoy, and keep on using Arch.
###
### License: GPL v2.0
###############################################################

prepare_base() {

	op_title="$install_op_msg"
	while (true)
	  do
		if "$UEFI" ; then
			bootloader=$(dialog --ok-button "$ok" --cancel-button "$cancel" --menu "$loader_type_msg" 13 64 4 \
				"grub"			"$loader_msg" \
				"syslinux"		"$loader_msg1" \
				"systemd-boot"	"$loader_msg2" \
				"efistub"	    "$loader_msg3" \
				"$none" "-" 3>&1 1>&2 2>&3)
			ex="$?"
		else
			bootloader=$(dialog --ok-button "$ok" --cancel-button "$cancel" --menu "$loader_type_msg" 12 64 3 \
				"grub"			"$loader_msg" \
				"syslinux"		"$loader_msg1" \
				"$none" "-" 3>&1 1>&2 2>&3)
			ex="$?"
		fi

		if [ "$ex" -gt "0" ]; then
			if (dialog --defaultno --yes-button "$yes" --no-button "$no" --yesno "\n$exit_msg" 10 60) then
				main_menu
			fi
		elif [ "$bootloader" == "systemd-boot" ]; then
			break
		elif [ "$bootloader" == "efistub" ]; then
			break
		elif [ "$bootloader" == "syslinux" ]; then
			if ! "$UEFI" ; then
				if (tune2fs -l /dev/"$BOOT" | grep "64bit" &> /dev/null); then
					if (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$syslinux_warn_msg" 11 60) then
						mnt=$(df | grep -w "$BOOT" | awk '{print $6}')
						(umount "$mnt"
						wipefs -a /dev/"$BOOT"
						mkfs.ext4 -O \^64bit /dev/"$BOOT"
						mount /dev/"$BOOT" "$mnt") &> /dev/null &
						pid=$! pri=0.1 msg="\n$boot_load \n\n \Z1> \Z2mkfs.ext4 -O ^64bit /dev/$BOOT\Zn" load
						base_install+="$bootloader "
						break
					fi
				else
					base_install+="$bootloader "
					break
				fi
			else
				base_install+="$bootloader "
				break
			fi
		elif [ "$bootloader" == "grub" ]; then
			base_install+="$bootloader "
			break
		else
			if (dialog --defaultno --yes-button "$yes" --no-button "$no" --yesno "\n$grub_warn_msg0" 10 60) then
				dialog --ok-button "$ok" --msgbox "\n$grub_warn_msg1" 10 60
				break
			fi
		fi
	done

	if "$UEFI" ; then
		base_install+="efibootmgr "
	fi

}

add_software() {

	op_title="$software_op_msg"
	if (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$software_msg0" 10 60) then

		while (true)
		  do
			unset software
			add_soft=true
			if ! "$skip" ; then
				software_menu=$(dialog --extra-button --extra-label "$install" --ok-button "$select" --cancel-button "$cancel" --menu "$software_type_msg" 20 63 11 \
					"$aar" "$aar_msg" \
					"$audio" "$audio_msg" \
					"$graphic" "$graphic_msg" \
					"$internet" "$internet_msg" \
					"$multimedia" "$multimedia_msg" \
					"$terminal" "$terminal_msg" \
					"$text_editor" "$text_editor_msg" \
					"$system" "$system_msg" \
					"$done_msg" "$install \Z2============>\Zn" 3>&1 1>&2 2>&3)
				ex="$?"

				if [ "$ex" -eq "1" ]; then
					if (dialog --yes-button "$yes" --no-button "$no" --defaultno --yesno "\n$software_warn_msg" 10 60) then
						break
					else
						add_soft=false
					fi
				elif [ "$ex" -eq "3" ]; then
					software_menu="$done_msg"
				elif [ "$software_menu" == "$aar" ] && ! "$aa_repo" ; then
					if (dialog --yes-button "$yes" --no-button "$no" --yesno "\n$aar_add_msg" 10 60) then
						echo "deb $repo_line" | tee -a /target/etc/apt/sources.list
						aa_repo=true

					else
						continue
					fi
				fi
			else
				skip=false
			fi

			case "$software_menu" in
				"$aar")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 17 60 7 \
						"aptitude"	"$aar1" ON \
						"yaourt"		"$aar3" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$audio")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 20 60 10 \
						"audacity"		"$audio0" OFF \
						"audacious"		"$audio1" OFF \
						"cmus"			"$audio2" OFF \
						"jack2"         "$audio3" OFF \
						"projectm"		"$audio4" OFF \
						"lmms"			"$audio5" OFF \
						"mpd"			"$audio6" OFF \
						"ncmpcpp"		"$audio7" OFF \
						"pianobar"		"$audio9" OFF \
						"pavucontrol"	"$audio8" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$internet")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 19 60 9 \
						"chromium"			"$net0" OFF \
						"filezilla"			"$net1" OFF \
						"firefox"			"$net2" OFF \
						"thunderbird"		"$net6" OFF \
						"hexchat"			"$net11" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi

					if (<<<"$software" grep "thunderbird" &>/dev/null) && [ -n "$bro" ] && [ "$bro" != "lv" ]; then
							software+=" thunderbird-i18n-$bro"
					fi
				;;
				"$graphic")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 17 63 7 \
						"darktable"		"$graphic1" OFF \
						"imagemagick"	"$graphic4" OFF \
						"pinta"			"$graphic5" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$multimedia")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 17 63 7 \
						"mpv"					"$media7" OFF \
						"multimedia-codecs"			"$media8" OFF \
						"smplayer"				"$media4" OFF \
						"vlc"         	   			"$media6" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi

					if (<<<"$software" grep "multimedia-codecs") then
						software=$(<<<"$software" sed 's/multimedia-codecs/gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly ffmpegthumbnailer gst-libav/')
					fi
				;;
				"$terminal")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 18 63 8 \
						"guake"             "$term1" OFF \
						"kmscon"			"$term2" OFF \
						"pantheon-terminal"	"$term3" OFF \
						"rxvt-unicode"      "$term4" OFF \
						"terminator"        "$term5" OFF \
						"xfce4-terminal"    "$term6" OFF \
						"yakuake"           "$term7" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$text_editor")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 18 60 8 \
						"atom"			"$edit7" OFF \
						"emacs"			"$edit0" OFF \
						"geany"			"$edit1" OFF \
						"gedit"			"$edit2" OFF \
						"gvim"			"$edit3" OFF \
						"mousepad"		"$edit4" OFF \
						"neovim"		"$edit5" OFF \
						"vim"			"$edit6" OFF 3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$system")
					software=$(dialog --ok-button "$ok" --cancel-button "$cancel" --checklist "$software_msg1" 20 65 10 \
						"apache"		"$sys1" OFF \
						"bc"			"$sys25" OFF \
						"git"			"$sys3" OFF \
						"gparted"		"$sys4" OFF \
						"openssh"		"$sys10" OFF \
						"tmux"			"$sys14" OFF \
						"tuxcmd"		"$sys15" OFF \
						"virtualbox"	"$sys16" OFF \
						"ufw"			"$sys17" ON  3>&1 1>&2 2>&3)
					if [ "$?" -gt "0" ]; then
						add_soft=false
					fi
				;;
				"$done_msg")
					if [ -z "$final_software" ]; then
						if (dialog --yes-button "$yes" --no-button "$no" --defaultno --yesno "\n$software_warn_msg" 10 60) then
							break
						else
							add_soft=false
						fi
					else
						download=$(echo "$final_software" | sed 's/\"//g' | tr ' ' '\n' | nl | sort -u -k2 | sort -n | cut -f2- | sed 's/$/ /g' | tr -d '\n')
						download_list=$(echo "$download" |  sed -e 's/^[ \t]*//')

						if ! "$menu_enter" ; then
							echo "$(date -u "+%F %H:%M") : Add software list: $download" >> "$log"
							base_install+="$download_list "
							unset final_software
							break
						else
							if (dialog --yes-button "$install" --no-button "$cancel" --yesno "\n$software_confirm_var1" 20 65) then
								arch-chroot "$ARCH" apt-get --force-yes --allow-unauthenticated update &>>"$log" &
								pid=$! pri=$(<<<"$down" sed 's/\..*$//') msg="\n$software_load_var" load_log
								arch-chroot "$ARCH" apt-get --force-yes --allow-unauthenticated --no-install-recommends install $(echo "$download") &>>"$log" &
								pid=$! pri=$(<<<"$down" sed 's/\..*$//') msg="\n$software_load_var" load_log
								echo "$(date -u "+%F %H:%M") : Finished installing software" >> "$log"
								unset final_software
								break
							else
								unset final_software
								add_soft=false
							fi
						fi

					fi
				;;
			esac

			if "$add_soft" ; then
				if [ -z "$software" ]; then
					if ! (dialog --yes-button "$yes" --no-button "$no" --defaultno --yesno "\n$software_noconfirm_msg ${software_menu}?" 10 60) then
						skip=true
					fi
				else
					add_software=$(echo "$software" | sed 's/\"//g')
					source "$lang_file"
					if (dialog --yes-button "$add" --no-button "$cancel" --yesno "\n$software_confirm_var0" 16 60) then
						final_software+="$software "
					fi
				fi
			fi
		done
	fi

}
