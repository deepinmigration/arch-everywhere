#!/bin/bash
###############################################################
### Arch Linux Anywhere Install Script
### configure_system.sh
###
### Copyright (C) 2017 Dylan Schacht
###
### By: Dylan Schacht (deadhead)
### Email: deadhead3492@gmail.com
### Webpage: http://arch-anywhere.org
###
### Any questions, comments, or bug reports may be sent to above
### email address. Enjoy, and keep on using Arch.
###
### License: GPL v2.0
###############################################################
generate_config(){
	ROOT_BLK=$(mount -l | grep '\s/target\s' | awk '{print $1}')
	BOOT_DEV=$(lsblk -drno PKNAME ${ROOT_BLK})
	deepin-installer-settings set "${DI_CONF}" "DI_BOOTLOADER" "/dev/${BOOT_DEV}"
	deepin-installer-settings set "${DI_CONF}" "DI_LOCALE" "zh_CN"
	deepin-installer-settings set "${DI_CONF}" "DI_TIMEZONE" "Asia/Shanghai"
	deepin-installer-settings set "${DI_CONF}" "system_info_setup_after_reboot" "true"
	deepin-installer-settings set "${DI_CONF}" "DISTRIBUTION" "debian"
	deepin-installer-settings set "${DI_CONF}" "LIVE" "live"
	deepin-installer-settings set "${DI_CONF}" "LIVE_FILESYSTEM" "/lib/live/mount/medium/live"
	deepin-installer-settings set "${DI_CONF}" "CDROM" "/lib/live/mount/medium"
	deepin-installer-settings set "${DI_CONF}" "DI_LUPIN_ROOT" "/lib/live/mount/findiso"
}

configure_system() {

	op_title="$config_op_msg"
	echo "$(date -u "+%F %H:%M") : Configure system: $LOCALE" >> "$log"

	generate_config
	cp "${DI_CONF}" /target/etc/
	mkdir -p "$ARCH"/deepinhost
	mount --bind / "$ARCH"/deepinhost
	mkdir -p "$ARCH"/media/cdrom
	mount --bind /lib/live/mount/medium "$ARCH"/media/cdrom
	rm -rf "${ARCH}"/root/hooks
	mkdir -p "$ARCH"/root/hooks/scripts
	cp -r "$aa_dir"/hooks/in_chroot/* $ARCH/root/hooks/scripts
	cp -a "$aa_dir"/hooks/basic_utils.sh $ARCH/root/hooks
	install -Dm755 "$aa_dir"/hooks/in_chroot.sh $ARCH/root/hooks/in_chroot.sh
	echo "$(date -u "+%F %H:%M") : Chroot system: $LOCALE" >> "$log"
	arch-chroot "$ARCH" /root/hooks/in_chroot.sh |tee -a /target/var/log/installer.log &
	pid=$! log=/target/var/log/installer.log msg="\n$software_load_var" load_log
	rm -rf "$ARCH"/root/hooks
	umount -f "$ARCH"/deepinhost
	export INSTALLED=true
}

set_hostname() {

	op_title="$host_op_msg"

	while (true)	## Begin set hostname loop
	  do		## Prompt user to enter hostname check for starting with numbers or containg special char
		hostname=$(dialog --ok-button "$ok" --nocancel --inputbox "\n$host_msg" 12 55 "deepin-pc" 3>&1 1>&2 2>&3 | sed 's/ //g')

		if (<<<$hostname grep "^[0-9]\|[\[\$\!\'\"\`\\|%&#@()+=<>~;:/?.,^{}]\|]" &> /dev/null); then
			dialog --ok-button "$ok" --msgbox "\n$host_err_msg" 10 60
		else
			break
		fi
	done

	echo "$hostname" > "$ARCH"/etc/hostname
	deepin-installer-settings set "${DI_CONF}" "DI_HOSTNAME" "$hostname"
	#Fix: Disable by leaeasy
	#user=root
	#set_password

}
